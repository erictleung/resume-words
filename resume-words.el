;;; resume-words.el --- Suggest strong words to include in your resume -*- lexical-binding: t -*-

;; Copyright (C) 2021  erictleung

;; Author: erictleung <Eric T Leung>
;; URL: https://gitlab.com/erictleung/resume-words
;; Keywords: convenience wp
;; Version: 0.1
;; Package-Requires: ((emacs "25.1") (dash "2.12.0"))

;; This file is not part of Emacs

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; resume-words
;; For more information, see https://gitlab.com/erictleung/resume-word

;; Note: this file is tangled from resume-words.org.
;; The .org contains documentation and notes on usage of the package.

;;; Code:

(require 'cl-lib)
(require 'ht)
(require 's)

(defvar resume-words--all-words
  (ht
   (:cat1 (ht (:title "Leadership or manager role")
              (:items (list
                       "Advised"
                       "Aligned"
                       "Arranged"
                       "Assisted"
                       "Authorized"
                       "Augmented"
                       "Centralized"
                       "Chaired"
                       "Championed"
                       "Coached"
                       "Counseled"
                       "Critiqued"
                       "Cultivated"
                       "Delegated"
                       "Developed"
                       "Directed"
                       "Educated"
                       "Enabled"
                       "Enforced"
                       "Evaluated"
                       "Executed"
                       "Facilitated"
                       "Fostered"
                       "Founded"
                       "Guided"
                       "Headed"
                       "Hosted"
                       "Inspired"
                       "Lead"
                       "Managed"
                       "Mentored"
                       "Mobilized"
                       "Monitored"
                       "Motivated"
                       "Orchestrated"
                       "Oversaw"
                       "Reviewed"
                       "Ran"
                       "Spearheaded"
                       "Supervised"
                       "Taught"
                       "Trained"
                       "United"))))
   (:cat2 (ht (:title "Sales or customer service role")
              (:items (list
                       "Accelerated"
                       "Accomplished"
                       "Advanced"
                       "Advised"
                       "Advocated"
                       "Achieved"
                       "Boosted"
                       "Built"
                       "Captured"
                       "Convinced"
                       "Corresponded"
                       "Delivered"
                       "Drove"
                       "Earned"
                       "Enhanced"
                       "Expanded"
                       "Fielded"
                       "Generated"
                       "Increased"
                       "Initiated"
                       "Maximized"
                       "Merged"
                       "Negotiated"
                       "Outperformed"
                       "Performed"
                       "Persuaded"
                       "Resolved"
                       "Stimulated"
                       "Sustained"
                       "Won"
                       "Yielded"))))
   (:cat3 (ht (:title "Communication or creative role")
              (:items (list
                       "Address"
                       "Advocated"
                       "Aided"
                       "Altered"
                       "Authored"
                       "Built"
                       "Clarified"
                       "Collaborated"
                       "Composed"
                       "Conceptualized"
                       "Constructed"
                       "Consulted"
                       "Conveyed"
                       "Convinced"
                       "Corrected"
                       "Corresponded"
                       "Crafted"
                       "Created"
                       "Defined"
                       "Demonstrated"
                       "Designed"
                       "Determined"
                       "Developed"
                       "Devised"
                       "Documented"
                       "Drafted"
                       "Edited"
                       "Energized"
                       "Enhanced"
                       "Established"
                       "Explained"
                       "Fashioned"
                       "Fielded"
                       "Fixed"
                       "Formulated"
                       "Guided"
                       "Illustrated"
                       "Influenced"
                       "Informed"
                       "Interacted"
                       "Initiated"
                       "Listened"
                       "Mediated"
                       "Moderated"
                       "Modified"
                       "Negotiated"
                       "Overhauled"
                       "Patched"
                       "Persuaded"
                       "Piloted"
                       "Pioneered"
                       "Prepared"
                       "Promoted"
                       "Publicized"
                       "Rebuilt"
                       "Resolved"
                       "Shared"
                       "Showcased"
                       "Transformed"
                       "Translated"
                       "Upgraded"
                       "Visualized"
                       "Wrote"))))
   (:cat4 (ht (:title "Technical role")
              (:items (list
                       "Advanced"
                       "Architected"
                       "Automated"
                       "Built"
                       "Coded"
                       "Deployed"
                       "Designed"
                       "Developed"
                       "Devised"
                       "Diagnozed"
                       "Discovered"
                       "Engineered"
                       "Enhanced"
                       "Expedited"
                       "Formulated"
                       "Installed"
                       "Implemented"
                       "Launched"
                       "Modified"
                       "Networked"
                       "Planned"
                       "Programmed"
                       "Remodeled"
                       "Rewrote"
                       "Refined"
                       "Tested"
                       "Transformed"
                       "Troubleshooted"
                       "Updated"
                       "Upgraded"))))
   (:cat5 (ht (:title "Project management role")
              (:items (list
                       "Coordinated"
                       "Completed"
                       "Delegated"
                       "Executed"
                       "Established"
                       "Facilitated"
                       "Forecasted"
                       "Formulated"
                       "Formalized"
                       "Guided"
                       "Handled"
                       "Implemented"
                       "Inspected"
                       "Initiated"
                       "Introduced"
                       "Launched"
                       "Managed"
                       "Mapped"
                       "Organized"
                       "Planned"
                       "Processed"
                       "Prioritized"
                       "Proposed"
                       "Reorganized"
                       "Reported"
                       "Represented"
                       "Solved"
                       "Spearheaded"
                       "Tested"
                       "Tracked"))))
   (:cat6 (ht (:title "Team player")
              (:items (list
                       "Acknowledged"
                       "Assimilated"
                       "Blended"
                       "Coalesced"
                       "Collaborated"
                       "Contributed"
                       "Diversified"
                       "Embraced"
                       "Encouraged"
                       "Energized"
                       "Gathered"
                       "Harmonized"
                       "Ignited"
                       "Joined"
                       "Melded"
                       "Participated"
                       "Partnered"
                       "United"
                       "Volunteered"))))
   (:cat7 (ht (:title "Researcher")
              (:items (list
                       "Analyzed"
                       "Assessed"
                       "Audited"
                       "Calculated"
                       "Checked"
                       "Discovered"
                       "Examined"
                       "Explored"
                       "Identified"
                       "Inspected"
                       "Investigated"
                       "Mapped"
                       "Measured"
                       "Probed"
                       "Proved"
                       "Quantified"
                       "Studied"
                       "Surveyed"
                       "Tested"
                       "Tracked"))))
   (:cat8 (ht (:title "Financial role")
              (:items (list
                       "Audited"
                       "Calculated"
                       "Classified"
                       "Collected"
                       "Equalized"
                       "Evaluated"
                       "Halted"
                       "Investigated"
                       "Lowered"
                       "Maintained"
                       "Maximized"
                       "Minimized"
                       "Recognized"
                       "Secured"))))
   (:cat9 (ht (:title "General achievement and accomplishments")
              (:items (list
                       "Accelerated"
                       "Accomplished"
                       "Achieved"
                       "Acquired"
                       "Acted as"
                       "Advanced"
                       "Amplified"
                       "Attained"
                       "Boosted"
                       "Capitalized"
                       "Chaired"
                       "Completed"
                       "Conserved"
                       "Consolidated"
                       "Created"
                       "Deciphered"
                       "Decreased"
                       "Delivered"
                       "Drove"
                       "Earned"
                       "Enacted"
                       "Enhanced"
                       "Exceeded"
                       "Executed"
                       "Expedited"
                       "Finished"
                       "Forged"
                       "Generated"
                       "Grew"
                       "Improved"
                       "Increased"
                       "Lifted"
                       "Made"
                       "Managed"
                       "Maximized"
                       "Navigated"
                       "Operated"
                       "Outpaced"
                       "Partnered"
                       "Produced"
                       "Reached"
                       "Reduced"
                       "Secured"
                       "Stimulated"
                       "Streamlined"
                       "Succeeded in"
                       "Surpassed"
                       "Undertook"
                       "Volunteered"
                       "Yielded"))))))

(defun resume-words--prompt-roles (words)
  "Get title groups from WORDS object."
  (ht-map (lambda (k _)
            (resume-words--ht-get* words k :title))
          words))

(defun resume-words--prompt-items (role &optional words)
  "Obtain words associated with ROLE from WORDS."
  (unless words (setq words resume-words--all-words))
  (resume-words--get-items role words))

(defun resume-words--ht-get* (table &rest keys)
  "Starting with TABLE, Look up KEYS in nested hash tables.
The lookup for each key should return another hash table, except
for the final key, which may return any value."
  (if (cdr keys)
      (apply #'resume-words--ht-get* (ht-get table (car keys)) (cdr keys))
    (ht-get table (car keys))))

(defun resume-words--get-items (role &optional words)
  "Get words for a given ROLE from object WORDS."
  (unless words (setq words resume-words--all-words))
  (resume-words--ht-get* words role :items))

(defun resume-words--get-role (res &optional words)
  "Retrieve words the role RES from object WORDS."
  (unless words (setq words resume-words--all-words))
  (let ((item (ht-find (lambda (_ v)
                         (equal (ht-get v :title)
                                res))
                       words)))
    (car item)))

(defun resume-words--insert (words)
  "Insert WORDS from hash table."
  ;; Get role, then words per role to choose from, then insert word.
  (let* ((role (completing-read "Choose a role: "
                                (resume-words--prompt-roles words) nil t))
         (rolewords (resume-words--get-role role words))
         (items (resume-words--prompt-items rolewords))
         (word (completing-read "Choose a word: " items nil t)))
    (insert word)))

;;;###autoload
(defun resume-words ()
  "Insert a words from a list of words by topic."
  (interactive)
  (resume-words--insert resume-words--all-words))

(provide 'resume-words)
;;; resume-words.el ends here
