# Resume-words

![https://www.gnu.org/licenses/gpl-3.0](https://img.shields.io/badge/License-GPL%20v3-blue.svg)

> Suggest strong works to include in your resume

When writing your resume, you may want to strong verbs to capture the
reviewer's attention.

This package is used by calling

```
M-x resume-words
```

## Acknowledgments

This work was heavily based on
[@nashamri](https://github.com/nashamri)'s great package
[academic-phrases](https://github.com/nashamri/academic-phrases).

The literate programming approach to creating this package was based on
[@EFLS](https://github.com/EFLS/)' great note taking package
[zetteldeft](https://github.com/EFLS/zetteldeft/).
